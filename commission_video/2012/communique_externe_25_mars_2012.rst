

.. index::
   pair: vidéo; Formation syndicale du SUB-RP
   pair: Formation; syndicale
   pair: Reprise; SCOP
   ! Grève


.. _formation_syndicale_2012:

======================================================
Formation syndicale du SUB-RP
======================================================

.. seealso::

   - http://www.cnt-f.org/video/


Le 20 janvier 2012, le syndicat du Bâtiment de Toulouse a invité le Syndicat
Unifié du Bâtiment et des Travaux Publics de la Région Parisienne (SUB-RP) afin
d'animer une formation syndicale.

La commission confédérale "Secteur Vidéo" de la CNT a filmé et monté de larges
extraits de cette formation. Voici les liens permettant de les visionner
et/ou de les télécharger sur le site "Caméra au poing" :*



Présentation des moyens et enjeux du SUB-RP
============================================

.. seealso::  http://www.cnt-f.org/video/videos/49-construction--btp/317-presentation-cnt-batiment-rp-partie01

Une vidéo de 12mn 23

Présentation du pourquoi et du comment de la permanence organisée par le SUB-RP
===============================================================================

.. seealso::  http://www.cnt-f.org/video/videos/49-construction--btp/323-presentation-cnt-batiment-rp-partie02

Une vidéo de 18mn 31

La qualité du travail
=====================

.. seealso::  http://www.cnt-f.org/video/videos/49-construction--btp/326-presentation-cnt-batiment-rp-partie03

Une vidéo de 5mn 26

La section syndicale (début)
============================

.. seealso::  http://www.cnt-f.org/video/videos/49-construction--btp/329-presentation-du-syndicat-unifie-du-batiment-de-la-region-parisienne-partie-4

Trois vidéos de 8mn 29, 16mn 02 et 6mn 23 abordant les thèmes suivants :

- articulation de la section syndicale avec le syndicat d'industrie,
- éventuelle délocalisation de l'entreprise dans laquelle cette section
  est implantée,
- façon dont une section syndicale peut préparer ou non les travailleurs
  licenciés à une reprise en SCOP, droits et devoirs au sein du SUB-RP,
  conditions de soutien du SUB-RP à une section syndicale en grève

La section syndicale (suite)
============================

.. seealso:: http://www.cnt-f.org/video/videos/49-construction--btp/331-presentation-cnt-batiment-rp-partie04-suite

Quatre vidéos de 13mn 48, 8mn 01, 14mn 33 et 15mn 35 abordant les thèmes
suivants:

- intérêt du regroupement en chambre syndicale, mandat et label,
- conscience de classe, conscience collective, conscience révolutionnaire,
- accord d'entreprise et représentativité
- nomination des conseillers du salarié,
- représentant de la section syndicale, délégué syndical, négociation annuelle,
  TPE (très petites entreprises)

En grève (histoire du droit de grève)
=====================================

.. seealso:: http://www.cnt-f.org/video/videos/49-construction--btp/337-presentation-du-syndicat-unifie-du-batiment-de-la-region-parisienne-partie-5

Une vidéo de 10mn 22 consacrée, après une courte introduction générale,
à l'historique du droit de grève de 1791 à 1982

En grève (aspects juridiques et pratiques de la grève)
=======================================================

.. seealso::  http://www.cnt-f.org/video/videos/49-construction--btp/339-presentation-du-syndicat-unifie-du-batiment-de-la-region-parisienne-suite-partie-5-et-fin

Deux vidéos de 27mn 33 et 29mn 23 abordant les thèmes suivants:

- conditions pour se mettre en grève,
- délai de préavis éventuel,
- information à l'employeur,
- grève perlée, grève tournante, grève du zèle, grève de solidarité,
- délit d'entrave, utilisation d'intérimaires pour
  casser la grève,
- frais occasionnés par la grève,
- piquet de grève,
- occupation du lieu de travail, séquestration du patron,
- sanctions éventuelles prises par l'employeur, retrait des jours de grève,
- seuil à partir duquel on est prêt à négocier
- collectif de grève,
- signature de la liste des revendications par les grévistes,
- choix du type de grève (débrayage, grève limitée ou illimitée),
- graduation des actions,
- soutien extérieur,
- choix de médiateurs,
- gestion du compte de grève,
- carte de grève,
- implication des familles dans le mouvement,
- rapports avec les médias,
- avocat, huissier, police,
- jaunes,
- négociations,
- vente sauvage du stock,
- poursuite de la production pendant la grève au profit de la
  caisse de grève
