

.. index::
   pair: vidéo; Au prix du gaz


.. _video_au_prix_du_gaz_2012:

======================================================
Vidéo "Au prix du gaz"  11 janvier 2012
======================================================

.. seealso::

   - http://www.cnt-f.org/video/
   - http://www.cnt-f.org/video/accueil/62-commerce-industrie-services/313-au-prix-du-gaz



Juillet 2009, Châtellerault. L'usine de sous-traitance automobile « New
Fabris » vient d'être mise en liquidation judiciaire. Les ouvriers
occupent l'usine. Ils envoient une dépêche à l'Agence France Presse : «
/Les bouteilles de gaz sont dans l'usine. Tout est prévu pour que ça
saute en l'absence d'accord au 31 juillet stipulant que chaque salarié
recevra 30.000 EUR de PSA et Renault. On ne va pas les laisser attendre
août ou septembre pour récupérer les pièces en stock et les machines
encore dans l'usine. Si, nous, on n'a rien, eux n'auront rien du tout ! /».

« Au prix du gaz » (film documentaire écrit et réalisé par Karel
Pairemaure) est une plongée au coeur de la lutte ouvrière, de la rage à
la reconstruction. Silence des machines. Paroles d'ouvriers. Un écho aux
« sans voix », à une classe ouvrière devenue invisible...

Bande-annonce à voir ici :
http://www.cnt-f.org/video/accueil/62-commerce-industrie-services/313-au-prix-du-gaz
