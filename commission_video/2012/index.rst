

.. index::
   pair: communiqués vidéo; 2012



======================================================
Communiqués du secteur vidéo  2012
======================================================


.. seealso::

   - http://www.cnt-f.org/video/


.. toctree::
   :maxdepth: 4

   communique_externe_25_mars_2012
   communique_externe_11_janvier_2012
