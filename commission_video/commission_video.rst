
.. index::
   pair: Commission; secteur vidéo
   pair: Courriel; relations-medias@cnt-f.org
   ! Vidéo


.. _commission_secteur_video_pub:

======================================
Video
======================================

.. seealso::

  - http://www.cnt-f.org/video/
  - https://twitter.com/#!/SecteurVideoCnt
  - :term:`Video`




Adresses courriel
=================

1. Maintenant pour toutes les questions et commentaires concernant le
   « Secteur Vidéo » utilisez l'adresse suivante: secteur-video@cnt-f.org



.. image:: secteur-video-cnt-image.jpg


Communiqués du secteur vidéo
============================


.. toctree::
   :maxdepth: 4

   outils/index
   2012/index
   2011/index
