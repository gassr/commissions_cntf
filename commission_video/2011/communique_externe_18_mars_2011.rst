

.. index::
   communiqué externe création site CNT vidéo


.. _creation_site_cnt_video_pub:

======================================================
Création du site vidéo de la CNT, 18 mars 2011
======================================================

.. seealso::

   - http://www.cnt-f.org/video/



Le « Secteur Vidéo » confédéral de la CNT, c'est quoi ?
========================================================

Depuis quelques années, des syndicalistes de la CNT produisent de la vidéo à
caractère syndical mais pas seulement.
Un grand nombre de ces vidéos est visible sur des sites d’hébergement gratuits
parfois peu recommandables mais, pour les « sans pognon », c'est encore le seul
moyen d'exister sur la Toile.

En octobre 2010, la CNT décide de se doter d'une commission confédérale
« Secteur Vidéo » qui  répond à un quadruple besoin :

- mettre un espace de travail collaboratif à disposition des « vidéastes »
  de la CNT,
- mutualiser leurs productions ainsi que leurs compétences techniques
  et syndicales,
- diffuser ces productions par le biais d’un site CNT spécifiquement dédié à
  la vidéo,
- s'opposer à la propagande de l'État et du patronat délivrée à travers
  les mass-médias.

En février 2011, sur la base de l'entraide, du volontariat et du savoir faire,
deux camarades du Syndicat de l'Industrie Informatique (SII-CNT) aident la
commission « Secteur Vidéo » à élaborer son site.

Karine en crée le logo et Salah en construit l'architecture et la maquette.

À cette époque, la CNT dispose déjà de plusieurs outils médiatiques.
Citons par exemple:

- un secteur « propagande » (pour la production et la diffusion d’affiches
  et d’autocollants),
- un secrétariat « de relations avec les médias » (pour la diffusion de
  communiqués de presse),
- un journal mensuel confédéral (« Le Combat syndicaliste »),
- une revue théorique confédérale (« Les Temps maudits »),
- des revues locales, régionales et fédérales (comme « N’Autre école », la
  revue de la fédération éducation).
- des émissions radiophoniques transmises par des stations associatives
  indépendantes dans plusieurs villes.

Depuis le 18 mars 2011, le site « Secteur Vidéo » s’ajoute à cette liste
d'outils médiatiques.

Présentement, nous constatons que la Confédération Nationale du Travail est
encore très marginalisée dans les mass-médias et que le traitement dont elle
fait l’objet sur le plan audio-visuel est souvent négatif ou erroné, voire
proche du « tout et n'importe quoi » !

Comme beaucoup d'autres sites CNT (syndicats, fédérations...),
le « Secteur Vidéo » te fera donc découvrir une autre réalité de la CNT par la
diffusion centralisée de vidéos présentant les différentes luttes dans
lesquelles ses syndicats se sont engagés en France, et dans le reste du monde
en lien avec ses organisations sœurs,

Déjà réalisées ou à venir, les productions vidéos de documentaires, films de
fiction et créations artistiques en lutte ont bien sûr toute leur place sur ce
site.
Tu y trouveras aussi de l'information purement syndicale ainsi que des liens
nous semblant cohérents avec notre démarche.

Le « Secteur Vidéo » est autogéré par des syndicalistes de la CNT sur leurs
heures de loisirs. Nous ne sommes pas des professionnels de la vidéo, mais de
simples militant.e.s luttant avec un caméscope au poing !

Tu peux commenter les vidéos présentes sur le site et/ou les télécharger
librement. Tu peux aussi nous en proposer d’autres en écrivant à  et nous te
répondrons très rapidement.

Bon visionnage « en rouge et noir » !

Confédération Nationale du Travail
Secteur Vidéo.
