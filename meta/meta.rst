.. index::
   ! meta-infos


.. _doc_meta_commissions_infos:

=====================
Meta
=====================

- https://cnt-f.gitlab.io/meta/




Gitlab project
================

- https://gitlab.com/cnt-f/commissions


Issues
--------

- https://gitlab.com/cnt-f/commissions/-/boards

Pipelines
-----------

- https://gitlab.com/cnt-f/commissions/-/pipelines

Membres
-----------

- https://gitlab.com/cnt-f/commissions/-/members

pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
