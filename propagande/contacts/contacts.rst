
.. index::
   pair: Propagande ; Contacts


.. _propa_contacts:

=============
Contacts
=============

Média Local Radio Alpes 1 <redaction@alpes1.com>,
20 minutes
Grenoble <20minutes-grenoble@pleinstitres.fr>,
Média Local Radio Couleur 3
<la.radio@couleur3.ch>,
Média Local Radio Fréquence 4 <frequence4@wanadoo.fr>,
Média Local Les Affiches <affiches-redac@affiches.fr>,
AFP Grenoble
<grenoble@afp.com>,
AFP Jean-Pierre Clatot <jean-pierre.clatot@afp.com>,
Média
Local AFP <antoine.agasse@afp.com>,
Média Local France 3 Alpes <alpes@francetv.fr>,
Média Local Le Travailleur Alpin <travailleur.alpin@wanadoo.fr>,
Média
AlterJT <contact@alterjt.tv>,
AMPHOUX Celia <celia.amphoux@ledauphine.com>,
Média Local Le Petit Bulletin <amartinez@petit-bulletin.fr>,
Dauphiné
C. Amphoux <celia.amphoux@ledauphine.com>,
Média Local Radio Campus
<grenoble@radio-campus.org>,
France tv C. Aubert <celine.aubert@francetv.fr>,
Celine Aubert <celine.aubert@francetv.fr>,
Média Local Radio Fréquence Commune
<frequencecommune@wanadoo.fr>,
Dau Eve Moulinier <eve.moulinier@ledauphine.com>,
Dau Hélène Delarroqua <helene.delarroqua@ledauphine.com>,
Dauphiné
Libéré <centre.grenoble@ledauphine.com>,
Dau Saléra Benarbia
<salera.benarbia@ledauphine.com>,
Média Local Radio Val d'Isère
<redaction@radiovaldisere.com>,
Média Local Bouy DL <benoit.bouy@ledauphine.com>,
Média Local Isabelle Calendre DL <isabelle.calendre@ledauphine.com>,
France tv D. Vigneau-dugue <denis.vigneau-dugue@francetv.fr>,
ECHINARD
Stephane <stephane.echinard@ledauphine.com>,
ESTRANGIN Matthieu
<matthieu.estrangin@ledauphine.com>,
Fakir Lyon <lyon@fakirpresse.info>,
FBI France Bleu Isère <bleuisere@radiofrance.com>,
FBI Véronique
Pueyo <veronique.pueyo@radiofrance.com>,
FBI Véronique Saviuc
<veronique.saviuc@radiofrance.com>,
Florent MATHIEU <fmathieu@placegrenet.fr>,
Média Local Radio Couleurs FM <contact@couleursfm.fr>,
Média Local Radio News FM
<redaction@radio-newsfm.fr>,
Média Local Radio Fontaine <radio_fontaine@bbox.fr>,
FOURGEAUD Gerard <gerard.fourgeaud@radiofrance.com>,
France 3 Céline Serrano
<celine.serrano@francetv.fr>,
France 3 <redactiongrenoble@francetv.fr>,
France 3 Patrick Pinto <patrick.pinto@francetv.fr>,
Média Local
Radio RTL, Le Parisien Et Aujourd'hui En France <serge.pueyo@free.fr>,
Radio France G. Fourgeaud <gerard.fourgeaud@radiofrance.com>,
Gre'net
Joël Kermabon <joel.kermabon@neptune.fr>,
Média Local Place Grenet
<contact@placegrenet.fr>,
Gre'net Séverine Cattiaux <severine@cattiaux.com>,
Média Local Club Com Grenoble <club.com.38@wanadoo.fr>,
Média Local
Indymedia Grenoble <indymediagrenoble@riseup.net>,
Média Local Radio
Campus Grenoble <contact@campusgrenoble.org>,
Média Local Rédaction
France 3 Grenoble <redactiongrenoble@france3.fr>,
Média Local Radio
Grésivaudan <redaction@radio-gresivaudan.org>,
Média Local Radio
Kol Hachalo <rkh@rkhfm.com>,
Ici Grenoble <contact@ici-grenoble.org>,
Média Local RCF Isère <rcfisere@gmail.com>,
Média Local Radio Italienne
<radio.italienne@wanadoo.fr>,
Dauphiné JB Vigny <jeanbenoit.vigny@ledauphine.com>,
FBI Rédac chef Léopold Strajnick <leopold.strajnic@radiofrance.com>,
Média Local Journal Le Postillon <lepostillon@yahoo.fr>,
Le tamis
<contact@le-tamis.info>,
Média Local Le Rouge Et Le Vert <ades@globenet.org>,
Radio france L. Strajnic <leopold.strajnic@radiofrance.com>,
Dauphiné
M. Estrangin <matthieu.estrangin@ledauphine.com>,
Média Local Particité
<jb.auduc@particite.fr>,
Gre'net Place <redaction@placegrenet.fr>,
Média Local Présences <presences@grenoble.cci.fr>,
Radio
Gresivaudan <teleauplacard@radio-gresivaudan.org>,
Radio Kaléidoscope
<redactionrks@yahoo.fr>,
Média Local Radio Hot Radio <redaction@hotradio.fr>,
Média Local Radio Virgin Radio <redac.grenoble@virginradio.fr>,
Média
Local Radio RCF <rcfisere@rcf.fr>,
Média Local Radio Oxygène Rédaction
<redaction@radiooxygene.com>,
Média Local Henry Reporter <francois.henry3@sfr.fr>,
Média Local France 3 Schmitt <xavier.schmitt@francetv.fr>,
Dauphiné
S. Echinard <stephane.echinard@ledauphine.com>,
Média Local Groupe Sept
<info@territorial.fr>,
Radio France T. Cattaneo <tommy.cattaneo@radiofrance.com>,
Télégrenoble <redaction@telegrenoble.net>,
France 3 Rédac cheffe
Vaitilingom <claudette.vaitilingom@francetv.fr>,
Les affiches V. Guilbert
<victor.guilbert@affiches.fr>,
Victor Guilbert <victor.guilbert@affiches.fr>,
VIGNEAU-DUGUÉ Denis <denis.vigneau-dugue@francetv.fr>,
VIGNY Jean Benoit
<jeanbenoit.vigny@ledauphine.com>,
Ici Grenoble <contact@ici-grenoble.org>,
Le postillon <lepostillon@gresille.org>,
Le Tamis <contact@le-tamis.info>,
Logement Diffusion <logement-diffusion@rezo.net>,
Logement Diffusion
<logement-diffusion@rezo.net>
