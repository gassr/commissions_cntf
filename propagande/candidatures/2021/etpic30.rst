

.. _propa_etpic30_2021:

=====================================================================================
Candidature au secrétariat à  la propagande ETPIC30 2021
=====================================================================================



::

    ---------------------------- Message original ----------------------------
    Objet:   [Liste-syndicats] Candidatures au secrétariat à  la propagande
    (Commission administrative)
    De:      "CNT 30" <cnt.30@cnt-f.org>
    Date:    Dim 22 novembre 2020 15:30
    À:       congres@cnt-f.org
    Copie à: CNT secrétariat confédéral <cnt@cnt-f.org>
             "Syndicats" <Liste-syndicats@bc.cnt-fr.org>
             cnt30@interconimes.cnt-fr.org
    --------------------------------------------------------------------------


Bonjour à toutes et tous,

Le syndicat ETPIC 30 présente la candidature de Kelly et Vincent pour
reprendre le mandat confédéral du Secteur propagande en binôme.

Le syndicat propose de continuer à gérer comme précédemment les
commandes et les impressions (affiches, autocollants, bandeaux,
drapeaux) mais aussi d'intégrer un volet de conception de nouveaux
visuels (qui seraient ensuite à confédéraliser via la procédure
définie).

Amitiés ASSR

Vincent, pour le secrétariat de l'ETPIC 30

--
CNT 30
06 rue d'Arnal, 30000 Nîmes
cnt.30@cnt-f.org/ 07 68 34 32 20
http://www.cnt-f.org/30

tw : @cnt_30 | fb : @cnt30gard | ig : @cnt_gard
