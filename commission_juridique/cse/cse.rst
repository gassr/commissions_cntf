
.. index::
   pair: CSE; Juridique

.. _cse:

======================================
CSE (Comité Social et Economique)
======================================

.. seealso::

  - :ref:`juridique_pub`
  - https://fr.wikipedia.org/wiki/Comit%C3%A9_social_et_%C3%A9conomique
  - http://www.cnt-f.org/spip.php?rubrique3 (vos droits)


Historique
==========

.. toctree::
   :maxdepth: 4

   2019/2019
