
.. index::
   pair: 19 juin 2013; Antifa


.. _comm_antifa_19_juin_2013_pub:

========================================================================================================
Antifasciste, antiraciste, oui... mais aussi anticapitaliste, autogestionnaire et internationaliste !
========================================================================================================

.. seealso::

   - :ref:`sipm_19_juin_2013`
