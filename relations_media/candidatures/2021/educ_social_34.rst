

.. _candidature_media_ess34:

=====================================================================================
Candidature "CNT ESS 34" <education34@cnt-f.org>
=====================================================================================



::

    ---------------------------- Message original ----------------------------
    Objet:   [Liste-syndicats] mandatement relations médias
    De:      "CNT ESS 34" <education34@cnt-f.org>
    Date:    Dim 22 novembre 2020 18:32
    --------------------------------------------------------------------------


Camarades,

Dans le cadre de la consultation référendaire des syndicats
organisée par le bureau confédéral, nous vous informons porter et
soutenir la candidature de Niko (ETPIC 30) au mandat de relation média.

Depuis le dernier congrès confédéral (novembre 2016), ce mandat est
vacant. Bien que notre camarade, en plus de son mandat de secrétaire
confédéral et en plus de s'occuper en partie des tâches dédiés au
secrétariat confédéral adjoint administratif (mandat vacant également),
s'est chargé des publications confédérales, nous constatons un manque de
régularité et de visibilité de la CNT dû à la carence de mandat.

C'est pour cela que nous portons et soutenons la candidature de Niko à
ce mandat. L'idée pour le camarade, est de produire des communiqués
courts et réguliers (si possible hebdomadaire) en lien avec l'actualité
social, mais aussi des communiqués mensuels qui pourraient servir
d'édito pour le CS. Il s'agit aussi pour le camarade, de travailler en
lien avec les différentes fédérations de la CNT et de s'investir au sein
du pôle média confédéral.

Salutations AS & SR,

Le syndicat Éducation Social-Services 34
