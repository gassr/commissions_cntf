
.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center


.. _comm_cnt_f:

====================================================================
**Commissions CNT-F**
====================================================================


.. toctree::
   :maxdepth: 3

   antifa/antifa
   antisexiste/antisexiste
   combat_syndicaliste/combat_syndicaliste
   commission_juridique/commission_juridique
   commission_video/commission_video
   international/international
   propagande/propagande
   relations_media/relations_media
   reseaux_sociaux/reseaux_sociaux
   textile/textile
   meta/meta
