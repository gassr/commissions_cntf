

.. _comm_reseaux_sociaux:

=====================================================================================
Réseaux sociaux
=====================================================================================


:Adresse courriel: reseauxsociaux@cnt-f.org


Description du mandat
========================

Reprendre les informations émanant des syndicats, des fédérations, du
secrétariat international et de la confédération

cela passe regarder régulièrement la liste syndicats, les sites et pages
officielles des syndicats (et de leur groupes d'actions) être sur la
liste des fédérations est un plus
être en contact avec le CS pour diffuser les couvertures
savoir convertir un pdf/word en jpeg (possible directement en ligne)
modérer et supprimer les commentaires qui n'ont rien à voir ou qui
appellent au contraire des publications

répondre aux messages (il n'y en a pas beaucoup, en moyenne 2 ou 3 par
mois, un message automatique repond à la demande en revoyant les
personnes sur l'annuaire des syndicats et fédés, sur la boite
contact@cnt-f.org et sur la boite du SI).

Aucun message privé haineux de fachos, de patrons, de complotistes depuis
2017 et très rarement dans les commentaires.
nous avons créer des albums avec les visuels (affiches et stickers),
mais il doit sans doute en manquer, si vous en avez pour compléter nous
sommes preneu.se.r.s
on les partage régulièrement avec un petit texte en fonction du moment

nous avons également créer des visuels sur l'actualité en reprenant les
valeurs communes qui nous animent
https://www.facebook.com/media/set/?vanity=138173166739441&set=a.406957713194317

la perfection n'existe pas, nous avons surement fait des erreurs ou
oublier de poster telle ou telle publication,
si c'est le cas nous nous en excusons auprès de vous
n'hésitez pas à nous en faire part

nous profitons de ce message pour faire un peu de pub sur deux pages
facebook qui parlent à leur manière de la CNT et qui commencent à avoir
de la portée sur la toile
nous ne sommes pas les propriétaires, mais nous pensons que ce mode de
lutte est super et qu'il montre que notre confédération n'est pas du
tout comme les autres
nous vous invitons donc à regarder ces deux pages parodiques, qui
utilisent l'humour pour faire passer des messages anarcho-syndicaliste
et syndicalistes révolutionnaire

- CNT section Police Nationale
- Confédération Memique du Travail
