#
# Commissions
#
import os
import platform
import sys
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx

source_suffix = ".rst"
master_doc = "index"
index_doc = "index"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
project = "Commissions"
copyright = f"2011-{now.year}, , Creative Commons CC BY-NC-SA 3.0. Built with Python {platform.python_version()}"
release = version
html_title = "Commissions"
html_short_title = html_title
html_logo = "cnt.jpg"
extensions = ["sphinx.ext.intersphinx"]
intersphinx_mapping = {
    "congres": ("https://cnt-f.gitlab.io/congres_confederaux/", None),
    "https://cnt-f.gitlab.io/syndicats/": None,
    "regles_conf": ("https://cnt-f.gitlab.io/statuts/", None),
    "http://france1.frama.io/juridique": None,
    "https://cnt-f.gitlab.io/rhone_alpes/": None,
    "https://cnt-f.gitlab.io/iledefrance/": None,
    "https://international.frama.io/cnt/": None,
    "https://cnt-f.gitlab.io/livret_accueil//": None,
}
extensions += ["sphinx.ext.todo"]
todo_include_todos = True
extensions += [
    # https://ablog.readthedocs.io/manual/markdown/
    "myst_nb",
]
# MyST config
myst_update_mathjax = False
myst_enable_extensions = []
myst_enable_extensions.append("colon_fence")  # myst_admonition_enable = True
myst_enable_extensions.append("deflist")  # myst_deflist_enable = True
extensions += [
    "ablog",
]
# https://ablog.readthedocs.io/manual/ablog-configuration-options/
#####################################################################
blog_path = "commissions"
# Base URL for the website, required for generating feeds.
blog_baseurl = "https://cnt-f.gitlab.io/commissions/"
blog_title = "commissions"
# Post related
# Date display format (default is '%b %d, %Y') for published posts
post_date_format = "%Y-%m-%d"
# Number of seconds (default is 5) that a redirect page waits before
# refreshing the page to redirect to the post
post_redirect_refresh = 1
# Index of the image that will be displayed in the excerpt of the post.
# Default is 0, meaning no image.
# Setting this to 1 will include the first image, when available, to the excerpt.
# This option can be set on a per post basis using post directive option image
post_auto_image = 1
# Number of paragraphs (default is 1) that will be displayed as an excerpt from the post
post_auto_excerpt = 4
# Blog feeds
blog_feed_archives = True
blog_feed_fulltext = True
blog_feed_subtitle = False
blog_feed_titles = False
# Specify number of recent posts to include in feeds, default is None for all posts
blog_feed_length = None
# Font awesome
# ABlog templates will use of Font Awesome icons if one of the following is set: fontawesome_link_cdn
fontawesome_included = True
# https://ablog.readthedocs.io/manual/posting-and-listing/?highlight=blog_post_pattern#posting-with-page-front-matter
# Instead of adding blogpost: true to each page, you may also provide a
# pattern (or list of patterns) in your conf.py file using the blog_post_pattern option
blog_post_pattern = "news/*/*/*"
html_extra_path = ["feed.xml"]
liste_full = [
    "sourcelink.html",
    "searchbox.html",
    "globaltoc.html",
    "postcard.html",
    "recentposts.html",
    "archives.html",
    "tagcloud.html",
    "categories.html",
]
html_sidebars = {
    "index": liste_full,
    "index/**": liste_full,
    "meta/**": liste_full,
    "antifa/**": liste_full,
    "antisexiste/**": liste_full,
    "combat_syndicaliste/**": liste_full,
    "commission_juridique/**": liste_full,
    "commission_video/**": liste_full,
    "international/**": liste_full,
    "propagande/**": liste_full,
    "relations_media/**": liste_full,
    "reseaux_sociaux/**": liste_full,
    "textile/**": liste_full,
    "tresorerie/**": liste_full,
}
extensions += [
    "sphinx_panels",
]
# Panels config
panels_add_bootstrap_css = False
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
pygments_style = "sphinx"
html_theme = "sphinx_book_theme"
html_theme_options = {
    "search_bar_text": "Search this site...",
    "search_bar_position": "navbar",
}
copyright = f"2011-{now.year}, , Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme=}"
