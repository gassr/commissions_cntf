

.. _candidature_textile_2021_interpro_07:

=========================================
Candidature Textile 2021 interpro 07
=========================================


::

    ---------------------------- Message original ----------------------------
    Objet:   [Liste-syndicats] candidature mandat Textile et administration
    Combat Syndicaliste
    De:      "Syndicat CNT Interpro. 07" <cntinterpro07@cnt-f.org>
    Date:    Mar 24 novembre 2020 19:31
    À:       congres@cnt-f.org
             cnt@cnt-f.org
    Copie à: "Liste Syndicats" <liste-syndicats@bc.cnt-fr.org>
    --------------------------------------------------------------------------

    Aux syndicats de la CNT:
    Notre syndicat se porte candidat pour reprendre le mandat du secteur
    textile .
    D'autre part, dans le cadre de la rotation des mandats, si un syndicat
    se propose pour reprendre le mandat de l'administration du C.S.
    c'est avec un grand plaisir que nous le lui transmettrons.Nous nous
    tenons Ã  la disposition
    des candidats éventuels pour toute information concernant les taches à
    effectuer pour l'administration du CS
    Si pas de candidat notre syndicat veut bien assurer un nouveau
    et*DERNIER *mandat pour l'administration du CS.

    Fraternelles salutations AS&SR
   milien secrÃ©taire

    --
    Syndicat CNT INTERPROFESSIONNEL de l'ArdÃ¨che
    Maison des Syndicats
    18 Aveneue de SIERRE
    07200 AUBENAS
    TEL 06 79 37 32 87
    www.cnt-f.org
    Permanence les merecredis de 17h30 Ã  19h
